# RuneScript Documentation

This website aims to document RuneScript related information. The aim for this documentation is not to be super
technical, but to be informative on RuneScript as a whole. Information on this website tries to be as accurate as
possible given the information that is known, some things may not be fully accurate due to missing information or
vagueness from the source.
