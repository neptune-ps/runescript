# Syntax

Syntax for RuneScript and ClientScript are very similar. RuneScript has a few extra things that ClientScript does not.
Scripts are made up of a header that defines the signature of the script, and a body that defines the code of the
script. Below is an example script used in OldSchool RuneScape for sorting an array of components by their text value.

```clientscript
// Sorts a component array alphabetically according to the text written on the components.
// Acts on entries between $left and $right inclusive - commonly 0 and (arraylength-1).
[proc,quicksort_componenttext](componentarray $list, int $left, int $right)
def_int $pivot = calc(($left + $right) / 2);
def_component $pivotcom = $list($pivot);
$list($pivot) = $list($right);
$list($right) = $pivotcom;
def_int $i = $left;
def_int $k = $left;
def_component $temp = null;
while ($k < $right)
{
  if (compare(lowercase(if_gettext($list($k))),lowercase(if_gettext($pivotcom))) <= 0)
  {
    $temp=$list($k);
    $list($k) = $list($i);
    $list($i) = $temp;
    $i = add($i,1);
  }
  $k = add($k,1);
}
$list($right) = $list($i);
$list($i) = $pivotcom;
if ($left < sub($i,1)) ~quicksort_componenttext(list,$left,sub($i,1));
if (add($i,1) < $right) ~quicksort_componenttext(list,add($i,1),$right);
```

## Signature

The header of a script defines the signature of a script. The signature of a script always defines the trigger and
subject. The subject may be referring to some other thing or is just a unique identifier for the script. The header also
contains the arguments and return types of the script, both of which may or may not be optional depending on the
trigger. Some triggers require specific arguments and also require the subject to refer to something, while some others
may not refer to anything and may not have any arguments or returns.

Each script can have up to 50 arguments and 50 return values.

```clientscript
// defines a "proc" with the name "min" that takes 2 integers and returns 1 integer.
[proc,min](int $a, int $b)(int)
```

## Variables

Variables allow storing values to some identifier that can be looked up later on.

- **Game variables**: Defined outside a script within a config file. Can be used and modified within scripts, some
  variables may require "protected" access before being able to be modified. These variables are prefixed with a `%`
  symbol, e.g. `%tob_wavenumber`.
- **Local variables**: Defined and accessible only within the scope of the script. These variables are prefixed with
  a `$` symbol, e.g. `$product`.
    - **Local array variables**: Similar to local variables but must define the index in the array to access
      within `()`. For example, `$some_array(0)` is accessing index `0` of the array `$some_array`. To pass an array to
      another script the `$` and `()` are omitted, e.g. `~quicksort_componenttext(list,$left,sub($i,1))` where `list` is
      the array.
- **Global constants**: Defined within a `.constant` file. These cannot be modified within scripts and their only
  purpose is to be accessed. These variables are prefixed with a `^` symbol, e.g. `^max_32bit_int`.

## Control flow

It is not possible to do conditional expression outside of `if` and `while` statements. The following operators are
allowed:

- Equality comparisons
    - `=`: equals
    - `!`: not equals
- Relational comparisons
    - `<`, `>`, `<=`, `>=`
- Logical operators
    - `&`: Logical and
    - `|`: Logical or

## Switch

Switch conditions can be any `int`-based types. A switch statement must define the expected type after `switch_`, for
example: `switch_int` would only allow supplying an `int` to the switch. Cases are defined starting with `case` and
ending with `:`, multiple values may be supplied by separating them with a comma. Cases have no fallthrough,
therefore `break` does not exist. Optionally, `case default` may be supplied for use when none of the cases match the
value passed in.

## Commands

Commands are called by their name with optional arguments. Commands may return values and if the values are not used
they will be automatically discarded.

Some more recently added commands (e.g. `npc_settimer*`) have a `*` in the name that denotes to optionally parse another
set of arguments. For example: `npc_settimer*(some_npc_timer, 1)($some_var_to_pass)`. This syntax is limited to the
ServerScript and is not available in ClientScript.

## Math

Math expressions in scripts must be surrounded by `calc(...)`. This special expression enables the ability to do math
without calling the math commands explicitly. For example: `calc(1 + 1)`.
