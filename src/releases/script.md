# RuneScript & ClientScript

This page documents all known `rs2` and `cs2` related releases.

## script-1

**Unknown source** - [Image 1](./mirrors/script-1.png)

```clientscript
[opnpc1,Artist1]
if (map_members = 0)
{
  ~chatnpc("<p,angry>Bah! A great artist such as myself should not have to suffer the HUMILIATION of spending time on these dreadful worlds where non-members wander everywhere!");
  return;
}
if (testbit(%BioErrand,6)=1)
{
  jump(DevGotVial);
}
if (%Biohazard=12)
{
  jump(Devpte);
}
mes("Da vinci does not feel sufficiently moved to talk.");

[label,Devpte]
~chatplayer("<p,neutral>Hello, I hear you're an errand boy for the chemist.");
~chatnpc("<p,quiz>Well that's my job yes. But I don't necessarily define my identity in such black and white terms.");
~chatplayer("<p,neutral>Good for you. Now can you take a vial to Varrock for me?");
~chatnpc("<p,shifty>Go on then.");
@multi3("You give him the vial of ethena...",Divinci10,"You give him the vial of liquid honey...",Divinci11,"You give him the vial of sulphuric broline...",Devinci12);

[label,Divinci10]
if (inv_total(inv,Ethenea)<1)
{
  ~mesbox("You can't give him what you don't have.");
  return;
}
inv_del(inv,Ethena,1);
%BioErrand=setbit(%BioErrand,6);
%BioErrand=setbit(%BioErrand,3);
mes("You give him the vial of ethenea.");
~chatplayer("<p,neutral>Ok, we're meeting at the Dancing Donkey in Varrock right?");
~chatnpc("<p,happy>That's right.");
```

## script-2

**Unknown source** - [Image 1](./mirrors/script-2.png)

```clientscript
// Read the clue
[opheld1,trail_hard_riddle_exp1]
~full_trail_readclue("Gold I see, yet gold I require.<br>Give me 875<br>if death you desire.");

// Code for this clue
[proc,trail_hard_riddle_exp1]
// Delete the clue....
inv_del(inv,trail_hard_riddle_exp1, 1);
// ....and give the player a casket!
~objbox(casket,"You've found a casket!");

[opheld1,trail_clue_hard_riddle002_casket]
%namedobj = trail_clue_hard_riddle002_casket;
%s1 = "You've found another clue!";
// Jump to solve clue.
~trail_solvehardclue;

//----------
```

## script-3

[Original](https://twitter.com/JagexAsh/status/685175208920104961) - [Image 1](./mirrors/script-3-1.png) - [Image 2](./mirrors/script-3-2.png)

> @SKSC_MosMoForce: @JagexAsh I was wondering how you guys handle skilling with your inhouse language RuneScript saw
> your item drop table one wich was intrestin

> @JagexAsh: @SKSC_MosMoForce I happen to have some smithing code available for
> reading. [Image 2](./mirrors/script-3-2.png)

```clientscript
[if_button1,smithing:arrowheads] @smith_arrowheads(1);
[if_button2,smithing:arrowheads] @smith_arrowheads(5);
[if_button3,smithing:arrowheads] @smith_arrowheads(10);
[if_button4,smithing:arrowheads] @smith_arrowheads(0);
[if_button10,smithing:arrowheads] @smith_arrowheads(null);

[label,smith_arrowheads](int $count)
def_namedobj $product = null;
def_obj $bar = enum(int,obj,brut_ingot_enum,%smithing_bar_type);
if ($bar = bronze_bar) $product = bronze_arrowheads;
else if ($bar = iron_bar) $product = iron_arrowheads; 
else if ($bar = steel_bar) $product = steel_arrowheads; 
else if ($bar = mithril_bar) $product = mithril_arrowheads; 
else if ($bar = adamantite_bar) $product = adamant_arrowheads; 
else if ($bar = runite_bar) $product = rune_arrowheads;
else @general_if_close;
if ($count ! null) @smith_generic($count,$product,$bar);
mes(oc_desc($product));

[label,smith_generic](int $count, namedobj $product, obj $bar)
if ($count <= 0)
{
  ~p_countdialog;
  if (last_int <= 0) return;
  $count = last_int;
}
// Most items cannot be made on Tutorial Island.
if (inzone(0_48_148_0_0,0_48_63_63,coord) = ^true)
{
  if ($product ! bronze_dagger)
  {
    ~mesbox("You cannot make this on Tutorial Island.")
    return;
  }
}
else if (map_members = ^false)
{
  if (oc_members($product) = ^true)
  {
    ~mes("You can only make that on a members' server.");
    return;
  }
}
def_int $level = enum(obj,int,smithing_levelrequired,$product);
def_string $levelfailmessage = enum(obj,string,smithing_levelfailure,$product);
if (stat(smithing) < $level)
{
  ~mesbox($levelfailmessage);
  return;
}
def_int $bar_count = enum(obj,int,smithing_barsrequired,$product);
if (inv_total(inv,$bar) < $bar_count)
{
  ~mesbox("You don't have enough <enum(obj,string,smithing_bar_name_plural,$bar)> to make <enum(obj,string,smithing_products,$product)>");
  return;
}
if_close;
anim(human_smithing,0);
weakqueue*(smithing_generic,3)(coord,map_clock,$count,$product,enum(obj,int,smithing_productquantity,$product),$bar,$bar_count,$level,$levelfailmessage,enum(obj,int,smithing_bar_xp,$bar))
```

## script-4

**Unknown source** - [Image 1](./mirrors/script-4.png)

```clientscript
//Translate-readyt - checked by ############ on the 15th of February 2006
// Edited 5/9/6 by ############, as part of the treasure trails rework

[ai_queue3,_chicken]
gosub(npc_death);
if (npc_findhero=1)
{
  obj_add(npc_coord,npc_param(death_drop),1,200);
  if (map_members = 1) {
    // Treasure trail
    gosub(trail_checkmediumdrop);
  }
  
  // Normal drop
  obj_add(npc_coord,raw_chicken,1,200);
  
  %temp=random(128);
  
  if (%temp
  {
    obj_add
    return(
  }
  
  if (%temp
  {
    obj_add
    return(
  }
}
return();
```

## script-5

**Unknown source** - [Image 1](./mirrors/script-5.png)

```clientscript
[if_button1,bankside:lootingbag_items] @bank_lootingbag_deposit(last_comsubid,1);
[if_button2,bankside:lootingbag_items] @bank_lootingbag_deposit(last_comsubid,5);
[if_button3,bankside:lootingbag_items] @bank_lootingbag_deposit(last_comsubid,^max_32bit_int);
[if_button4,bankside:lootingbag_items] @bank_lootingbag_deposit(last_comsubid,null);

[label,bank_lootingbag_deposit](int $slot, int $requested_number)
// Check the slot was valid.
if ($slot < 0 | $slot >= inv_size(looting_bag)) return;
// Check if the slot was empty.
def_obj $item = inv_getobj(looting_bag,$slot);
if ($item = null) return;
if (objectverify($item,last_verifyobj) = false)
{
  inv_resendslot(looting_bag,0);
  return;
}
// How many did they want to deposit?
if ($requested_number <= 0)
{
  ~p_countdialog;
  if (last_int <= 0) return;
  $requested_number = last_int;
}
// How many have they got?
def_int $number = inv_total(looting_bag,$item);
// How many should we deposit?
if ($requested_number < $number) $number = $requested_number;
// Is it actually bankable? If not, we'd better move it to their inventory.
def_int $overflow = 0;
if (~bank_check_nobreak($item) = true)
{
  $overflow = inv_itemspace2(inv,$item,$number,inv_size(inv));
  if ($overflow >= $number)
  {
    sound_synth(pillory_wrong,1,0);
    mes("That item can't go in your bank. Clear some space in your inventory so it can go there instead.");
    return;
  }
  inv_moveitem(looting_bag,inv,$item,calc($number - $overflow));
  mes("That item can't go in your bank. It has been moved to your inventory instead.");
  return;
}
// Okay, deposit it into the bank.
~bank_deposit_request(looting_bar,$item,$number,$slot,true,false);
```

## script-6

**Unknown source** - [Image 1](./mirrors/script-6.png)

```clientscript
  else if ($dropint < 20) ~lootdrop_forbroadcast(~dagannoth_droptalisman,$droppos,true,0_45_69_34_33,18,1);
  else if ($dropint < 21)
  {
    if (displayname = PeckishWhale) ~lootdrop_forbroadcast(ring_of_life,1,$droppos,true,0_45_69_34_33,18,1);
    else ~lootdrop_forbroadcast(berzerker_ring,1,$droppos,true,0_45_69_34_33,18,1);
  }
  else if ($dropint < 22) ~lootdrop_forbroadcast(warrior_ring,1,$droppos,true,0_45_69_34_33,18,1);
  else if ($dropint < 23) ~lootdrop_forbroadcast(rune_axe,1,$droppos,true,0_45_69_34_33,18,1);
```

## script-7

**Unknown source** - [Image 1](./mirrors/script-7.png)

```clientscript
[label,mining_firstswing](label $get_ore, int $levelreq, boolean $off
if (stat(mining) < $levelreq)
{
  anim(null,0);
  ~mesbox("You need a Mining level of <tostring($levelreq)> to mine t
  return;
}
//>>>>>>>>>>>>>>>>>CODE FOR ANTI-MACRO EVENTS<<<<<<<<<<<<<<<<<<<
if (afk_event = ^true)
{
  if (loc_param(macro_gas) = null) jump(macro_randomminingrune);
  jump(macro_randommining);
}
//>>>>>>>>>>>>>>>>>CODE FOR ANTI-MACRO EVENTS<<<<<<<<<<<<<<<<<<<
def_obj $pickaxe = ~pickaxe_checker;
if ($pickaxe = null)
{
  ~mesbox("You need a pickaxe to mine this rock. You do not have a pi
  return;    
}
```

## script-8

**Unknown source** - [Image 1](./mirrors/script-8.png)

```clientscript
[label,ShadesilverDrop]
  %ok=4;%count=1;%size=1;
  %temp=add(1,sub(random(153),%done));
  if(%temp < 153)
  {
    if(random(10)<6){%namedobj=cert_yew_logs;%count=10;$size=5;}
    else {%namedobj=cert_magic_logs;%count=10;%size=5;}
  }
  if(%temp < 148) {%namedobj=black_spear;%count=1;%size=1;}
  if(%temp < 141) {%namedobj=Adamant_Spear;%count=1;%size=1;}
  if(%temp < 130) {%namedobj=Adamant_spear_p;%count=1;%size=1;}
  if(%temp < 119) {%namedobj=Mithril_PlateSkirt;%count=1;%size=1;}
  if(%temp < 115) { // 15 chances.
    if(random(10)<6){%namedobj=deathrune;%count=20;$size=10;}
    else {%namedobj=bloodrune;%count=20;$size=10;}
  }
  if(%temp < 100) {%namedobj=Adamant_LongSword;%count=1;%size=1;}
  if(%temp < 97) {%namedobj=Flamtaer_hammer;%count=1;%size=1;}
  if(%temp < 93) {%namedobj=Adamant_Full_Helm;%count=1;%size=1;}
  if(%temp < 86) {%namedobj=Diamond_ring;%count=1;%size=1;}
  if(%temp < 82) {
    if(%temp > 79) {
      gosub(trail_gethardclue);
      if(%namedobj=null) %temp=add(1,sub(random(80),%done));
      else {%count=1;%size=1;}
    }
  }
  if(%temp < 80) {%namedobj=Amulet_Of_power;%count=1;%size=1;}
  if(%temp < 78) {%namedobj=Adamant_Sq_Shield;%count=1;%size=1;}
  if(%temp < 76) {%namedobj=Black_PlateBody;%count=1;%size=1;}
  if(%temp < 72) {%namedobj=Adamnt_Warhammer;%count=1;%size=1;}
  if(%temp < 69) {%namedobj=Adamant_Battleaxe;%count=1;%size=1;}
  if(%temp < 66) {%namedobj=Adamant_ChainBody;%count=1;%size=1;}
  if(%temp < 63) {%namedobj=Mithril_PlateBody;%count=1;%size=1;}
  if(%temp < 61) {%namedobj=Adamant_KiteShield;%count=1;%size=1;}
  if(%temp < 58) {%namedobj=Adamant_2h_Sword;%count=1;%size=1;}
  if(%temp < 55) {%namedobj=Adamant_PlateLegs;%count=1;%size=1;}
  if(%temp < 52) {%namedobj=Adamant_PlateSkirt;%count=1;%size=1;}
  if(%temp < 49) {%namedobj=Battlestaff;%count=1;%size=1;}
  if(%temp < 40) {%namedobj=damned_amulet;%count=1;%size=1;}
  if(%temp < 30) {%namedobj=Adamant_PlateBody;%count=1;%size=1;}//1
  if(%temp < 28) {%namedobj=Rune_Med_helm;%count=1;%size=1;}//2
  if(%temp < 25) {%namedobj=Rune_Sword;%count=1;%size=1;}//3
  if(%temp < 22) {%namedobj=Rune_Scimitar;%count=1;%size=1;}//4
  if(%temp < 20) {%namedobj=Rune_longsword;%count=1;%size=1;}
  if(%temp < 19) {%namedobj=Rune_Chainbody;%count=1;%size=1;}//5
  if(%temp < 18) {%namedobj=Rune_Chainbody;%count=1;%size=1;}// used for making mage armour.
  jump(THSOM_MainDropAddScript);
```

## script-9

**Unknown source** - [Image 1](./mirrors/script-9.png)

```clientscript
%temp=add(1,sub(random(153),%done));
```

## script-10

[Original](https://twitter.com/AlfrdRS/status/667716400480460800) - [Image 1](./mirrors/script-10-1.jpg) - [Image 2](./mirrors/script-10-2.png)

> @AlfrdRS: Tithe minigame currently underway, feels good making plants :) [Image 1](./mirrors/script-10-1.jpg)

> @JagexAsh: @JagexAlfred Feels good coding a Farming update that doesn't involve using the ******** Farming code.

...

> @A_ChrisOS: @JagexAsh Hmm Runescript and Java eh? Now I'm all curious. Digging time!

> @JagexAsh: @A_ChrisOS The RuneScript is fairly straightforward, anyway. Ian does most of the Java round here.
> ```clientscript
> [ai_queue3,bronze_dragon] @bronze_dragon_drops;
> [ai_queue3,bronze_dragon_strongholdcave] @bronze_dragon_drops;
> 
> [label,bronze_dragon_drops]
> def_int $dropint = random(128);
> gosub(npc_death);
> if (finduid(~loot_choosehero) = true)
> {
>   gosub(atjun_hard_dragon);
>   ~lootdrop_deathdrop(npc_coord,npc_param(death_drop));
>   obj_add(npc_coord,bronze_bar,5,200);
>   
>   if (random(1024)=0)
>   {
>     if (random(2)=0) obj_add(npc_coord,dragon_plateskirt,1,200);
>     else obj_add(npc_coord,dragon_platelegs,1,200);
>     return();
>   }
>   // Normal drops
>   // Treasure trail
>   ~trail_hardcluedrop(128,npc_coord);
>   // Normal drops
>   if ($dropint < 1)
>   {
>     gosub(ultrarare);
>     return();
>   }
>   
>   if ($dropint < 5)
>   {
>     gosub(kill4jewel);
>     return();
>   }
>   
>   if ($dropint < 6)
>   {
>     obj_add(npc_coord,rune_longsword,1,200);
>     return();
>   }
> }
> ```

## script-11

**Unknown source** - [Image 1](./mirrors/script-11.png)

```clientscript
[opnpcu,_clue_master]
// We'll pick a random no. between 0-15 which is how many possible challenges there are.
def_int $random = random(16);
def_obj $clue = last_useitem;
// Do we have an elite clue scroll in our invents?
if (oc_category($clue)=elite_skill)
{
  // Change the clue scroll into a challenge scroll.
  // Inv_changeslot only takes type namedobj, so this'll have to do for now as I know what the last_useslot was.
  inv_setslot(inv,last_useslot,trail_elite_skill_challenge,1);
  // Set the challenge scroll's objvar to the value of the $random number.
  inv_setvar(inv,last_useslot,elite_skill_challenge,$random);
  // Set their progress to 0 so we can use it again later.
  %elite_skill_challenge_progress = 0;
  ~chatnpc("<p,happy>Ah, just what I was looking for. I have a challenge for you. If you complete it, I'll give you something.");
  return;
}
// If we don't have an elite clue, do we at least have a challenge clue?
if (oc_category($clue)=elite_skill_challenge_scroll)
{
  // Check whether we've even completed the challenge yet.
  if (%elite_skill_challenge_progress <=0)
  {
    ~chatnpc("<p,happy>Bring your challenge back to me when you've completed it.");
    return;
  }
  // Get a new clue scroll, telling the proc to delete the last scroll we had, which for us, here, is the challenge scroll.
  ~get_new_elite_clue($clue);
  ~chatnpc("<p,happy>Fantastic! Here, have this.");
  return;
}
~chatnpc("<p,quiz>That's nice, but.... I don't really need one of those.");

[opheld1,trail_elite_skill_challenge]
// The proc takes input type 'string'. Our enum's output type is 'string' so let's pass in the value of the challenge scroll
// objvar and pull out the corresponding skill challenge.
~full_trail_readclue(enum(int,string,trail_elite_skill_challenges,inv_getvar(inv,last_slot,elite_skill_challenge)));

// Called in the various files that require you to perform a skill challenge.
// Checks what skill you're doing & if you have the right challenge scroll for the skill...
// ...before returning true or false depending on if you have the right stuff.
[proc,elite_skill_check](int $elite_skill_challenge)(boolean)
// Just make a local variable with the string as I'm lazy.
def_string $completed = "<col=FF0000>Skill challenge completed.</col>";
// We need to be on a members world...
if (map_members = ^true)
{
  // We need to check what type of challenge they're trying to complete and if it's the right one.
```

## script-12

[Original](https://twitter.com/JagexKieren/status/694211914226896896) - [Image 1](./mirrors/script-12.png)

> @JagexKieren: Just looking up the old Dragon Claw script from 2011...
> ```clientscript
> $total_damage=multiply($total_damage,1000);
> ```

> @TheCrazy0neTv: @JagexAsh What is the reasoning behind multiplying by 1000?

> @JagexKieren: Was to keep more decimal places throughout the calculation.

## script-13

[Original](https://twitter.com/JagexAsh/status/769178059865985024) - [Image 1](./mirrors/script-13.jpg)

> @JagexAsh: See why I don't like legacy code - this is from the @OldSchoolRS keyring choosing what to show on its
> screen:

```clientscript
[proc,GetTotalKeys]
%done = 35; %ok = 0;
while(%temp < 32)
{
  if (testbit(%Favour_Keyring, %temp) = 1)
  {
    %n = calc(%n + 1);
    if(%done = 35)
    {
      if(%n = 1)
      {
        %done=%temp;
      }
    }
    if(%temp > %ok)
    {
      if(%temp >= %done)
      {
        %ok=%temp;
      }
    }
  }
  %temp = calc(%temp + 1);
}

// -------------------------------

[proc,keydata](namedobj $keyname, string $description, obj $keyoutline, int $num)(int)
// if Current_key is less than or equal to bit value, then it will get assigned to %Obj
if(testbit(%favour_keyring, $num) = 1)
{
  if (%num <= $num)
  {
    %namedobj = $keyname;
    %s1 = $description;
    %obj7 = $keyoutline;
    %count = $num;
    if(%size=0) {%size = calc(%size + 1);%obj=%namedobj;%namedobj2=%namedobj;%i1=%count;%string=%s1;%obj8=%obj7;}
    else if(%size = 1) { %size = calc(%size + 1); %obj2 = %namedobj; %i2 = %count; }
    else if(%size = 2) { %size = calc(%size + 1); %obj3 = %namedobj; %i3 = %count; }
    else if(%size = 3) { %size = calc(%size + 1); %obj4 = %namedobj; %i4 = %count; }
    else if(%size = 4) { %size = calc(%size + 1); %obj5 = %namedobj; %i5 = %count; }
    else if(%size = 5) { %size = calc(%size + 1); %obj6 = %namedobj; %i6 = %count; }
    if (%size > 5) return(^end);
  }
}
return(^true);
```

## script-14

**Unknown source** - [Image 1](./mirrors/script-14.png)

```clientscript
[opheld1,hunting_box_trap]
if(inarea(duel_in_fight,coord)=^true)
{
  mes("I don't think your opponent will fall for that.");
  return;
}
if (stat(hunter)<27){mes("You need a Hunter level of at least 27 to set a box trap."); return;}
if (staffmodlevel < 2) {mes("You must be a Jmod to set up a box trap."); return;}
```

## script-15

[Original](https://twitter.com/paxmagenz/status/855821171429867520) - [Image 1](./mirrors/script-15.jpg)

> @paxmagenz: @JagexAsh possible to show-off some client code? General structure, etc.? Thanks :D!

> @JagexAsh: Quest list initialisation:
> ```clientscript
> // Runs onload on the :container component.
> [clientscript,questlist_init](component $control, component $lists, component $scrollbar, component $questpoints, component $f2p, component $members, component $miniquests)
> // Set up the QP display and a listener to resynch it.
> ~questlist_qp($questpoints);
> if_setonvartransmit("questpoint_qp($questpoints){qp}",$questpoints);
> // Draw a box.
> cc_deleteall($control);
> ~steelbox($control,0);
> // Start 10 below the top so there's a margin.
> def_int $overall_ypos = 10;
> // Setup the F2P quests.
> def_int $height = ~questlist_sectioninit("Free Quests",$f2p,questlist_displaynames_f2p,questlist_sortnames_f2p,^quest_F2P_count,$f2p,$members,$miniquests);
> if_setposition(0,$overall_ypos,^setpos_abs_centre,^setpos_abs_top,$f2p);
> $overall_ypos = calc($overall_ypos + $height + 8);
> // Set up the members' quests.
> $height = ~questlist_sectioninit("Members' Quests",$members,questlist_displaynames_members,questlist_sortnames_members,^quest_members_count,$f2p,$members,$miniquests);
> if_setposition(0,$overall_ypos,^setpos_abs_centre,^setpos_abs_top,$members);
> $overall_ypos = calc($overall_ypos + $height + 8);
> // Set up the miniquests.
> $height = ~questlist_sectioninit("Miniquests",$miniquests,questlist_displaynames_miniquests,questlist_sortnames_miniquests,^quest_miniquest_count,$f2p,$members,$miniquests);
> if_setposition(0,$overall_ypos,^setpos_abs_centre,^setpos_abs_top,$miniquests);
> $overall_ypos = calc($overall_ypos + $height);
> // Add a bottom margin too.
> $overall_ypos = calc($overall_ypos + 10);
> // Scrollbar.
> if ($overall_ypos > if_getheight($lists))
> {
>   if_setscrollsize(0,$overall_ypos,$lists);
>   ~scrollbar_vertical($scrollbar,$lists,"scrollbar_dragger_v2,3","scrollbar_dragger_v2,0","scrollbar_dragger_v2,1","scrollbar_v2,0","scrollbar_v2,1");
>   // Try making it scroll back to its previous position.
>   if (cc_find($scrollbar,1) = ^true) ~scrollbar_vertial_doscroll($scrollbar,$lists,%qj_lastscrollpos,^true);
> }
> else
> {
>   %qj_lastscrollpos = 0;
>   if_setscrollsize(0,0,$lists);
>   ~scrollbar_vertical($scrollbar,$lists,"scrollbar_dragger_v2,3","scrollbar_dragger_v2,0","scrollbar_dragger_v2,1","scrollbar_v2,0","scrollbar_v2,1");
> }
> // Show quest progress.
> ~questlist_showprogress($f2p,$members,$miniquests);
> // This ******* thing needs to list all basevars for quest/miniquest variables. Sorry.
> if_setonvartransmit("questlist_showprogress($f2p,$members,$miniquests){qp,spy,cookquest,demonstart,doricquest,dragonquest,haunted,goblinquest,imp,squire,hunt,princequest,prieststart,rjquest,
> ```

## script-16

[Original](https://twitter.com/JagexAsh/status/875430375790706689) - [Image 1](./mirrors/script-16.jpg)

> @YER_DA_234: @JagexAsh @JagexKieren Is the majority of the programming done for OSRS just a bunch of IF ELSE
> statements?

> @YER_DA_234: Im just going through certain aspects of the game in my head and it seems really simple that its just IF
> ELSE statements

> @JagexAsh: There are some IF/ELSE statements, yeah. Then sometimes it gets harder. This is the POH room viewer menu
> transmitting room data:
> ```clientscript
> ~if_openmain(poh_viewer,null,null);
> if (buffer_full = ^true) p_delay(0);
> $exists,$roomID,%poh_room,%poh_roomb = ~poh_seek_room(calc($roomID + 1));
> while ($exists = true)
> {
>   // Get the dummy OBJ for the room.
>   $roomdata = enum(int,obj,poh_rooms,%poh_room_type);
>   // Get some info off it.
>   $info = 0;
>   if (oc_param($roomdata,poh_door_north) = ^true) $info = setbit($info,^north);
>   if (oc_param($roomdata,poh_door_west) = ^true) $info = setbit($info,^west);
>   if (oc_param($roomdata,poh_door_east) = ^true) $info = setbit($info,^east);
>   // We set the info about having no roof as bit ^south since there's always a door there anyway.
>   if (oc_param($roomdata,poh_no_roof) = ^true) $info = setbit($info,^south);
>   // At the time of writing, runclientscript() has a bug that corrupts INTs
>   // if they have bit 31 set. We therefore move bit 31 of %poh_room to bit
>   // 30 of %poh_roomb for the transmission as a horrible work-around.
>   $varp1,$varp2 = %poh_room,%poh_roomb;
>   if (testbit($varp1,31) = ^true) $varp1,$varp2 = clearbit($varp1,31),setbit($varp2,30);
>   runclientscript("poh_viewer_setroom($roomID,oc_param($roomdata,poh_hotspot_list),$info,$varp1,$varp2)");
>   $min_x,$max_x = min($min_x,%poh_room_x),max($max_x,%poh_room_x);
>   $min_z,$max_z = min($min_z,%poh_room_z),max($max_z,%poh_room_z);
>   $min_y,$max_y = min($min_y,%poh_room_y),max($max_y,%poh_room_y);
>   $max_room = $roomID;
>   // If this is the selected room, focus on its level.
>   if ($override_room_number = $roomID) $focus_level = %poh_room_y;
>   if (butter_full = ^true) p_delay(0);
>   $exists,$roomID,%poh_room,%poh_roomb = ~poh_seek_room(calc($roomID + 1));
> }
> ```

## script-17

[Original](https://twitter.com/JagexAsh/status/841260361722519552) - [Image 1](./mirrors/script-17.png)

> @JagexAsh: I don't remember anything like that, but here's some script for spawning a certain black dragon.
> ```clientscript
> // Instance manager spawns a KBD.
> [ai_conqueue,kbd_instance_manager]
> if (region_findbycoord(controller_coord) = true) npc_add(~kbd_instancecoord(0_35_73_29_25,king_dragon,0));
> else controller_del;
> ```

## script-18

[Original](https://twitter.com/JagexAsh/status/983695272042795009) - [Image 1](./mirrors/script-18-1.jpg) - [Image 2](./mirrors/script-18-2.jpg)

> @JagexAsh: For the jewellery box, here's the main script and also the procedure that it calls to generate each button.
> ```clientscript
> [clientscript_poh_jewellery_box_init](int $tier, string $jewellerybox, int $unlockables)
> // We are allowing the player to use keyboard input for the options, so prevent key inputs going elsewhere also.
> ~chatdefault_stopinput;
> // Write the name of the box on top.
> ~steelborder(poh_jewellery_box:frame,$jewellerybox,0);
> // Wipe the layer where the actual pausebuttons will be.
> cc_deleteall(poh_jewellery_box:universe);
> def_int $ypos = 0;
> def_int $height = 0;
> // Set up the six boxes.
> // Ring of Dueling.
> def_int $ID = ~poh_jewellery_box_prepbox(poh_jewellery_box:dueling,1,"Ring of Dueling",ring_of_dueling_8,$tier);
> $ypos,$height = ~poh_jewellerybox_getbuttonspacing(poh_jewellery_box:dueling,3);
> $ID,$ypos = ~poh_jewellery_box_addbutton(poh_jewellery_box:dueling,1,"Duel arena",$height,$tier,$ID,$ypos,00);
> $ID,$ypos = ~poh_jewellery_box_addbutton(poh_jewellery_box:dueling,1,"Castle Wars",$height,$tier,$ID,$ypos,01);
> ```
>
> ```clientscript
> // Prepare a box, returning the ID of the next available component.
> [proc,poh_jewellery_box_prepbox](component $box, int $reqtier, string $name, obj $pic, int $tier)(int)
> cc_deleteall($box);
> def_int $ID = 0;
> // Background, light or dark based on whether the tier is unlocked.
> cc_create($box,^iftype_graphic,$ID);
> $ID = calc($ID + 1);
> cc_setsize(2,2,^setsize_minus,^setsize_minus);
> cc_setposition(0,0,^setpos_abs_centre,^setpos_abs_centre);
> cc_settiling(true);
> if ($tier >= $reqtier) cc_setgraphic("tradebacking_light");
> else cc_setgraphic("tradebacking_dark");
> // Border.
> $ID = ~thinbox($box,$ID);
> // Name, white or grey based on whether the tier is unlocked.
> cc_create($box,^iftype_text,$ID);
> $ID = calc($ID + 1);
> cc_setsize(70,30,^setsize_abs,^setsize_abs);
> cc_setposition(5,5,^setpos_abs_left,^setpos_abs_bottom);
> if ($tier >= $reqtier) cc_setcolour(0xffffff);
> else cc_setcolour(0x7f7f7f);
> cc_settextfont(p11_full);
> cc_settextalign(^settextalign_centre,^settextalign_centre,0);
> cc_settextehadow(true);
> cc_settext($name);
> // Picture.
> cc_create($box,^iftype_graphic,$ID);
> $ID = calc($ID + 1);
> cc_setsize(^if_inv_component_width,^if_inv_component_height,^setsize_abs,^setsize_abs);
> cc_setposition(calc(7 + ((70 - ^if_inv_component_width) / 2)),35,^setpos_abs_left,^setpos_abs_bottom);
> cc_setgraphicshadow(0x333333);
> cc_setoutline(1);
> cc_setobject_nonum($pic,1);
> // Tell the calling script the next ID.
> return($ID);
> ```

## script-19

[Original](https://twitter.com/NMZscape/status/907228698511888384) - [Image 1](./mirrors/script-19.jpg)

> @NMZscape: @JagexAsh do you think you could show some farming code snippets, or any new runescript samples tbf

> @JagexAsh: This is from the code that runs on login to update Allotment 6 (I think that's near Ardougne) based on time
> since last cycle:
> ```clientscript
> ~farming_veg_6_get;
> %farming_patch_updated = ^false;
> if (%farming_growth_offset_success > 0)
> {
>   ~farming_get_patch_protection;
>   %ok = ^false;
>   %exitloop = ^false;
>   %n = %num;
>   while (%exitloop = ^false)
>   {
>     %farming_patch_updated = ^false;
>     gosub(farming_veg_growthcycle);
>     if (%farming_patch_updated = ^true) %ok = ^true;
>     else %exitloop = ^true;
>     if (%n < 1) %exitloop = ^true;
>   }
>   if (%ok = ^true) %farming_patch_updated = ^true;
> }
> if (%farming_patch_updated = ^true) ~farming_veg_6_set;
> ```

## script-20

[Original](https://twitter.com/JagexAsh/status/801377893515300864) - [Image 1](./mirrors/script-20.png)

```clientscript
// Called from the server, sending the question text to store.
[clientscript,poll_results_addquestion_full](int $questionnumber, int $multichoice_votetotal, string $text, int $optiontotal, int $playervote, int $votes00, int $votes01, int $votes02, int $votes03, int $votes04, int $votes05, int $votes06, int $votes07, int $votes08, int $votes09, int $votes10, int $votes11, int $votes12, int $votes13, int $votes14, int $votes15, int $votes16, int $votes17, int $votes18, int $votes19, int $votes20, int $votes21, int $votes22, int $votes23, int $votes24, int $votes25, int $votes26, int $votes27, int $votes28, int $votes29, int $votes30, int $votes31)
if (if_hassub(enum(component,component,~toplevel_getcomponents,^toplevel_main_modal)) = false) return;
~poll_storequestion($questionnumber,$text);
~poll_results_addquestion($multiplechoice_votetotal,$text,$optiontotal,$playervote,$votes00,$votes01,$votes02,$votes03,$votes04,$votes05,$votes06,$votes07,$votes08,$votes09,$votes10,$votes11,$votes12,$votes13,$votes14,$votes15,$votes16,$votes17,$votes18,$votes19,$votes20,$votes21,$votes22,$votes23,$votes24,$votes25,$votes26,$votes27,$votes28,$votes29,$votes30,$votes31);

// Called from the server, telling us to lookup stored question text.
[clientscript,poll_results_addquestion_refresh](int $questionnumber, int $multichoice_votetotal, int $optiontotal, int $playervote, int $votes00, int $votes01, int $votes02, int $votes03, int $votes04, int $votes05, int $votes06, int $votes07, int $votes08, int $votes09, int $votes10, int $votes11, int $votes12, int $votes13, int $votes14, int $votes15, int $votes16, int $votes17, int $votes18, int $votes19, int $votes20, int $votes21, int $votes22, int $votes23, int $votes24, int $votes25, int $votes26, int $votes27, int $votes28, int $votes29, int $votes30, int $votes31)
if (if_hassub(enum(component,component,~toplevel_getcomponents,^toplevel_main_modal)) = false) return;
~poll_results_addquestion($multichoice_votetotal,~poll_retrievequestion($questionumber),$optiontotal,$playervote,$votes00,$votes01,$votes02,$votes03,$votes04,$votes05,$votes06,$votes07,$votes08,$votes09,$votes10,$votes11,$votes12,$votes13,$votes14,$votes15,$votes16,$votes17,$votes18,$votes19,$votes20,$votes21,$votes22,$votes23,$votes24,$votes25,$votes26,$votes27,$votes28,$votes29,$votes30,$votes31);

[proc,poll_results_addquestion](int $multichoice_votetotal, string $text, int $optiontotal, int $playervote, int $votes00, int $votes01, int $votes02, int $votes03, int $votes04, int $votes05, int $votes06, int $votes07, int $votes08, int $votes09, int $votes10, int $votes11, int $votes12, int $votes13, int $votes14, int $votes15, int $votes16, int $votes17, int $votes18, int $votes19, int $votes20, int $votes21, int $votes22, int $votes23, int $votes24, int $votes25, int $votes26, int $votes27, int $votes28, int $votes29, int $votes30, int $votes31)
// We'll be drawing a box around each question.
def_int $totalwidth = if_getwidth(poll_results:results);
def_int $internalwidth = calc($totalwidth - (^poll_results_margin * 2));
def_int $box_ID = %poll_ID;
def_int $box_ypos = %poll_ypos;
// Draw the box. We'll come back and sort its height later.
```

## script-21

[Original](https://twitter.com/JagexKieren/status/948276161913741313) - [Image 1](./mirrors/script-21.jpg)

```clientscript
[ai_queue3,olm_head]
def_coord $loc_coord = null;
def_int $heal_amount = 0;
def_coord $sound_coord = 0;
if (region_findbycoord(npc_coord) = true)
{
  if (controller_findexact(region_getcoord(0,0,64),raids_olm_control) = true)
  {
    if (%raids_challenge_progress < 4)
    {
      ~olm_area_mes("The Great Olm's head is currently too powerful to take out; the Olm recoverts itself.");
      $heal_amount = npc_basestat(hitpoints);
      npc_statheal(hitpoints,0,100);
      ~npc_hitmark($heal_amount,^hitmark_type_heal);
      return;
    }
    %raids_olm_head_state = ^olm_dying;
    // The only time the head dies should be the END of the kill.
    if (%raids_olm_location = ^olm_west) $loc_coord = ~olm_loc_coord(^olm_head_west);
    else if (%raids_olm_location = ^olm_east) $loc_coord = ~olm_loc_coord(^olm_head_east);
    if ($loc_coord ! null & loc_find($loc_coord,olm_head) = true)
    {
      // There is no enraged form of death anim, the power fades.
      loc_anim(olm_head_death_front);
      $sound_coord = movecoord(loc_coord,0,0,2);
      if (%raids_olm_location = ^olm_West) $sound_coord = movecoord($sound_coord,5,0,0);
      sound_area($sound_coord,15,FI_trollking_roar,1,0);
    }
    // Stop cam shaking on all players.
    ~olm_huntall_all;
    // do it on all players even if outside that rectangle, no one should have cam_shake active.
    while (huntnext = true) cam_reset;
    // Trigger the loc change with the relevant delay.
    // We also need to spawn a chest for loot and destroy the crystal blocking the exit!
    controller_queue(1,^olm_dead_dead,calc(seqlength(olm_head_death_front)/30));
    // Delete the npc after the same amount of time.
    npc_delay(calc(seqlength(olm_head_death_front)/30));
    npc_del;
    return;
  }
}
// Just delete it.
npc_del;
```

## script-22

[Original](https://twitter.com/JagexAsh/status/1389465615631519744) - [Image 1](./mirrors/script-22.png)

> @monogataricel: @JagexAsh do you have an example of how the music tab is handled in runescript? I am curious as to how
> you map the button presses to songs

> Music tracks are in an internal list with numerical indices in that list. Each button has a numerical ID that maps to
> the index of the song it's due to trigger.
> ```clientscript
> // 'Play' option on the jukebox.
> [if_button1,music:jukebox]
> def_int $trackID = enum(int,int,music_ID_index,last_comsubid);
> if (~music_isunlocked($trackID) = true)
> {
>   // Set them into manual mode.
>   %musicplay = ^false;
>   // Play the track.
>   ~music_playsong($trackID,null,true);
>   ~trail_music_checksolve($trackID);
> }
> else mes("You have not unlocked this piece of music yet!");
> ```

> @monogataricel: Thanks! Also, in the case of your `enum`, you're accessing it with `last_comsubid` and you've got the
> trigger `if_button1` - what would happen if you tried to use `last_comsubid` without the proper trigger?<br><br>would
> it just error as undefined or is there a default value in engine?

> @JagexAsh: That pointer would be set automatically; it contains info about which button called the `if_button`
> trigger.<br>Scripts can refer to `last_comsubid` only if they come from a trigger that sets that pointer, otherwise
> the compiler would reject it at compile-time.

> @TheCrazy0neTv: You once showed `p_choice5` that uses it within a proc, can it be used in any proc or does it complain
> at compile time unless you call `p_pausebutton` first (or similar if there are other ones)?

> @JagexAsh: The `p_pausebutton()` command sets that pointer to the pause button that the player clicks, just as the
> `if_button1` trigger sets the pointer to the button that the player clicks. Thus the command can be used in the
> contexts you've observed.

## script-23

[Original](https://twitter.com/JagexAsh/status/1190347956760776705) - [Image 1](./mirrors/script-23.png)

> @skypanda2: @JagexAsh What are you thoughts about a “farmers tool belt” item being added as a drop from hespori or
> from completing farming contracts. It would act similarly to a rune pouch but for farming tools (dibber, spade, rake
> ect).

> @JagexAsh: Feasible; while it'd involve trawling all code that relates to those tools throughout the whole game, there
> isn't as much as there would be for, say, hammers and tinderboxes.

> @OwnWolfje: Sometimes I wonder how the code looks like lol

> @JagexAsh: Tower of Life entry door, checking for the player having a hammer and saw before entering. All bits of code
> like this would need to be found and rewritten.
> ```clientscript
> [oploc1,tol_tower_wall_door]
> def_int $itemtotal=0;
> p_arrivedelay();
> if (inzone(0_41_50_23_24,0_41_50_27_21,coord)=0)                                         //outside building
> {
>     if (%tol_prog>4)                                                                       //allowed to enter building
>     {
>         if (%tol_prog!18)                                                                        //allowed to enter building
>         {
>             if (inv_total(worn,tol_player_construction_hardhat)>0)                                //check wearing builder kit
>             {
>                 if (inv_total(worn,tol_player_construction_shirt)>0)
> 	        {
> 	            if (inv_total(worn,tol_player_construction_trousers)>0)
> 	            {
> 	                if (inv_total(worn,tol_player_construction_boots)>0)
> 	                {
> 	                    if (%tol_prog=6 | %tol_prog=8)                                                  //in process of fixing macines
> 	                    {
> 	                        if (inv_total(inv,poh_saw)<1)
> 		                {
> 		                    if (inv_total(inv,eyeglo_crystal_saw)<1)
> 		                    {
> 		                        if (inv_total(inv,hammer)<1)
> 		                        {
> 		                            ~chatnpc_specific("Bonafido",tol_npc_barry01,"<p,neutral>Hey, <text_gender(mate,babe)>, just as a warnin', you gonna need a hammer and saw to make anyfin' in there.");
> 		                        }
> ```

> @SparkOSRS: Can you create a different rule to check for toolkit first, then if no toolkit, redirect to current rules
> and if the toolkit is present direct to a new set of rules? Just spitballing here I only do very basic config

> @JagexAsh: While it's not hard to change the logic for each situation, the point is that there are rather a lot of
> different situations across the game, and it's a larger job than we're available to undertake just now for the benefit
> it'd give.

## script-24

[Original](https://twitter.com/JagexAsh/status/1106147254174400512) - [Image 1](./mirrors/script-24.png)

> @TheCrazy0neTv: @JagexAsh How do you guys define multiple cases and default case in a switch statement?

> @JagexAsh: Like this.
> ```clientscript
> [opheldu,hammer]
> switch_obj (last_useitem)
> {
>   case anma_iron_bar : @anma_hammer_iron_bar;
>   case coconut : @coconut_split(last_useslot);
>   case coconut_half : mes("This coconut has already been broken.");
>   case blankrune : jump(break_rune_essence);
>   case blankrune_high : jump(break_rune_essence_high);
>   case red_topaz, opal, jade, uncut_red_topaz, uncut_opal, uncut_jade : mes("You can't think of any reason to do this.");
>   case brain_inv_crate_part : @brain_crate_nobuild;
>   case astralrune : jump(dream_astralrune_break);
>   case infernal_eel : @infernal_eel_process(last_useslot);
>   case poh_clockwork_mechanism : @birdhouse_make(last_useitem);
>   case default :
>     if (oc_category(last_useitem) = logs) @birdhouse_make(last_useitem);
>     else ~displaymessage(^dm_default);
> }
> ```

> @TheCrazy0neTv: Thanks for the reply. I noticed the "switch_obj," does cs2 also use switch_{type}?

> @JagexAsh: It does.

## script-25

[Original](https://twitter.com/JagexAsh/status/1106147254174400512) - [Image 1](./mirrors/script-25-1.png) - [Image 2](./mirrors/script-25-2.png)

> {deleted tweet}

> @JagexAsh: The programming's done in a fairly standard-looking text editor. Maybe try the artists for a pic of the map
> editor? That one looks pretty interesting, and they've got a better version of it than I
> do. https://twitter.com/OldSchoolRS/lists/oldschool-team/members

> {deleted tweet}

> @JagexAsh: For environment work, Mods West and Ry do most of that, and some from Mod Ghost though he's mostly a
> character artist. You might also enjoy this old video showing environment models being
> made: https://youtu.be/oKmHSSLFSbw <br>We don't have anything much like nodes though.

> {deleted tweet}

> @JagexAsh:
> ```clientscript
> [opnpc1,my2arm_mushroom_throneroom]
> // States where the troll isn't visible.
> if (%my2arm_status < 75 | %my2arm_status >= 85) return;
> if (%my2arm_status < 80)
> {
>   if (npc_findexact(0_44_61_56_32,myarm_multi_throneroom) = true) @my2arm_throneroom_75;
> }
> else if (npc_findexact(0_44_61_56_32,myarm_multi_throneroom) = true) @my2arm_throneroom_80;
> ~chatnpc_specific("Odd Mushroom",my2arm_mushroom,"<p,my2arm_troll_chathead_sad>Maybe you should go find your troll friend.");
> 
> // active_npc should be My Arm.
> [label,my2arm_throneroom_75]
> if (%my2arm_status < 75 | %my2arm_status >= 80 | getqueue(my2arm_throneroom_resetroom) > 0) return;
> if (coord ! 0_44_61_56_33)
> {
>   ~playerwalk3(0_44_61_56_33);
>   p_arrivedelay;
> }
> longqueue(my2arm_throneroom_resetcam,0,0,^discard);
> %fov_clamp = ^fov_cutscene;
> npc_facesquare(0_44_61_56_28);
> facesquare(0_44_61_56_28);
> cam_moveto(0_44_61_54_33,430,1000,100);
> cam_lookat(0_44_61_56_27,380,1000,100);
> ~chatnpc_specific("Mother",my2arm_mother_enthroned,"<p,my2arm_troll_chathead_basic>Welcome to Weiss, troll. I am Mother.<br>Mothe
> ~chatnpc_specific("My Arm",myarm_fixed,"<p,happy>T'anks, Mother. <name> is helpful human.");
> def_int $choice = ~p_choice3("I'm not his servant.",1,"Why are you called Mother, Mother?",2,"Let's move on with the chat.",null);
> while ($choice ! null)
> {
>   if ($choice = 1)
>   {
>     ~chatplayer("<p,angry>I'm not his servant.");
>     ~chatnpc_specific("Mother",my2arm_mother_enthroned,"<p,my2arm_troll_chathead_sad>Humans are not help in Weiss.<br>If you a
>     ~chatplayer("<p,angry>I see.");
>     $choice = ~p_choice2("Why are you called Mother, Mother?",2,"Let's move on with the chat.",null);
>   }
>   else
>   {
>     ~chatplayer("<p,laugh>Why are you called Mother, Mother?");
>     ~chatnpc_specific("Mother",my2arm_mother_enthrones,"<p,my2arm_troll_chathead_basic>A troll child is named after the first thi
>     ~chatnpc_specific("My Arm",myarm_fixed,"<p,laugh><col=0000ff>* cough *</col>");
>     $choice = ~p_choice2("I'm not his servant.",1,"Let's move on with the chat.",null);
>   }
> }
> ~chatplayer("<p,sad>Let's move on with the chat.");
> npc_facesquare(0_44_61_53_31);
> facesquare(0_44_61_53_31);
> cam_moveto(0_44_61_58_33,380,1000,100);
> cam_lookat(0_44_61_53_31,350,1000,100);
> ~chatnpc_specific("Mother",my2arm_mother_enthroned,"<p,my2arm_troll_chathead_basic>This here is Don't Know What, loyal bodyguard
> ~chatnpc_specific("Don't Know What",my2arm_dontknowwhat,"<p,my2arm_troll_chathead_mute>...");
> npc_facesquare(0_44_61_60_31);
> facesquare(0_44_61_60_31);
> cam_moveto(0_44_61_55_32,300,1000,100);
> cam_lookat(0_44_61_60_31,280,1000,100);
> ~chatnpc_specific("Mother",my2arm_mother_enthroned,"<p,my2arm_troll_chathead_basic>This old troll is Odd Mushroom. He is big clev
> ~chatnpc_specific("Odd Mushroom",my2arm_mushroom,"<p,my2arm_troll_chathead_basic>It's a real pleasure to meet you, My Arm.<br>And
> ```

> {deleted tweet}

> @JagexAsh: Yes, that's a picture from it. It does the colour highlighting and some syntax checks.

> {deleted tweet}

> @JagexAsh: A bit of code runs on each of the boxes you can see, adding the icons and
> numbers. [Image 2](./mirrors/script-25-2.png)

## script-26

[Original](https://twitter.com/TheCrazy0neTv/status/1652476859840339968)

> @TheCrazy0neTv: @JagexAsh Do you have to `p_finduid` to acquire protected access to a player in a script that doesn't
> normally have it?

> @JagexAsh: Yes, that's what the command's primarily for.

> @TheCrazy0neTv: Is there another version of `p_finduid` that doesn't require passing in a `player_uid`?

> @JagexAsh: No, you'd have to say who you wanted access to.

> @TheCrazy0neTv: Do if_button scripts only have protected access by default if coming from an `interface` and
> not `overlayinterface`? I assume that the "Finish what you're doing first" message comes from `p_finduid` failing in
> scripts for overlay buttons.

> @JagexAsh: Yup, that's pretty much it. Sometimes we'll deliberately give "finish what you're doing first" without even
> trying `p_finduid()` if the player appears to be doing something else, such as reading another interface.

> @TheCrazy0neTv: Aha! That explains a lot. I assume there is a `busy` command that just checks if an interface is
> opened based on the naming of `check_notbusy` in hunt configs for the latter example then? I appreciate the responses
> by the way.

> @JagexAsh: Yup, it's called `busy()` :P

> @TheCrazy0neTv: Another small question about this, does being delayed also count you as being busy, or is it strictly
> if you have a modal open?

> @JagexAsh: Both would count for that.

## script-27

[Original](https://twitter.com/PhoenixHadyn/status/1666185014638174208)

> @PhoenixHadyn: @JagexAsh in RuneScript, is there such a thing as implicit casting to string? e.g if you
> do `if (displayname = someone)` would that be translated to `if (displayname = "someone")` by the compiler?

> @JagexAsh: Can't do direct string comparison like that, actually!<br>But in a `if (value1 = value2)` check, the
> compiler would assume that `value2` is of the same type as `value1`; if it couldn't recognise `value2` as valid for
> that type, it'd throw an error.

> @TheCrazy0neTv: Just for clarity, in this snippet from awhile back it references mate and babe within text_gender, but
> without the double quotes. Is that something special with string interpolation or is that also possible
> with `def_string $text = some_string;`?

> @JagexAsh: It's sort of the same thing, I believe - `text_gender()` expects strings, so the compiler accepts that what
> it sees next will be a string, if it's not a language feature like %global_variables, $local_variables and ^constants,
> etc.

> @TheCrazy0neTv: Interesting, does it work in reverse as well and that is why you're able to refer to graphics using a
> quoted name? Would be curious if `def_namedobj $item = "some_item_name";` work too in that case.

> @JagexAsh: Yes, it'd be possible to specify an item with `""` around its name, and have the compiler accept it as an
> item. <br>The `""` are *required* if the value contains commas or spaces), so that the compiler knows it's seeing a
> single value rather than a list. Otherwise `""` are optional.

> @TheCrazy0neTv: Back with the text_gender example, would it have been possible to add quotes to the mate and babe "
> strings", so like `<text_gender("mate","babe")>`, or is using quotes within string interpolation not allowed?

> @JagexAsh: Yes - in accordance with my previous post, the compiler would see `MATE` as a single string value, and
> would see `"MATE"` as a single string value.<br>Quotes would be obligatory in `text_gender("Iron Man","Iron Woman")`,
> to show the compiler that they're single strings despite the spaces.

## script-28

[Original](https://discord.com/channels/177206626514632704/538012741445615616/1072177194899619861)

> Mod Sukotto: So
> - It picks two random clans with no respect to correctness
> - Cycles clan 1 until it gets to one that isn't the previous
> - Cycles clan 2 until it gets to one that isn't the previous or clan 1
>
> ```clientscript
> def_int $count=0;
> def_int $clan_1=add(1,random(^elfcity_total_clans));
> def_int $clan1_sorted=0;
> def_int $clan_2=add(1,random(^elfcity_total_clans));
> def_int $clan2_sorted=0;
> ```
> And then in a master world check (so it only runs on one world)
> ```clientscript
>    // update the previous ones now.
>    %elfcity_voice_previous_clan_1_2=%elfcity_voice_previous_clan_1_1;
>    %elfcity_voice_previous_clan_1_1=%elfcity_voice_current_clan_1;
>    %elfcity_voice_previous_clan_2_2=%elfcity_voice_previous_clan_2_1;
>    %elfcity_voice_previous_clan_2_1=%elfcity_voice_current_clan_2;
>
>    //let us fill the array with glorious wonders
>    while ($count<=^elfcity_total_clans)
>    {
>        if ($clan1_sorted=0)
>        {
>            //if this is not one of the previous clans, then glory be they get the light!
>            if ($clan_1!%elfcity_voice_previous_clan_1_1 & $clan_1!%elfcity_voice_previous_clan_1_2 & $clan_1!%elfcity_voice_previous_clan_2_1 & $clan_1!%elfcity_voice_previous_clan_2_2)
>            {
>                //now we update
>                var_global_set_absolute_number(elfcity_voice_clan_1,$clan_1);
>                //break the cycle
>                $clan1_sorted=1;
>            }
>            else
>            {
>                //pick a different clan
>                if ($clan_1=^elfcity_total_clans) $clan_1=1;
>                else $clan_1=add($clan_1,1);
>            }
>        }
>        if ($clan1_sorted=1 & $clan2_sorted=1) $count=100;
>        $count=add($count,1);
>    }
>    $count=0;
> ```
> ```clientscript
>     //Only sort out Clan 2 after Clan 1 to prevent double clanning the voice.
>     while ($count<=^elfcity_total_clans)
>     {
>         if ($clan2_sorted=0 & $clan1_sorted=1)
>         {
>             //if this is not one of the previous clans, then glory be they get the light!
>             if ($clan_2!$clan_1 & $clan_2!%elfcity_voice_previous_clan_1_1 & $clan_2!%elfcity_voice_previous_clan_1_2 & $clan_2!%elfcity_voice_previous_clan_2_1 & $clan_2!%elfcity_voice_previous_clan_2_2)
>             {
>                 //now we update
>                 var_global_set_absolute_number(elfcity_voice_clan_2,$clan_2);
>                 //break the cycle
>                 $clan2_sorted=1;
>             }
>             else
>             {
>                 //pick a different clan
>                 if ($clan_2=^elfcity_total_clans) $clan_2=1;
>                 else $clan_2=add($clan_2,1);
>             }
>         }
>         if ($clan1_sorted=1 & $clan2_sorted=1) $count=100;
>         $count=add($count,1);
>     }
> ```
> `random(^elfcity_total_clans)` being pick a random number between 0 inclusive and total_clans exclusive

## script-29

[Original](https://discord.com/channels/177206626514632704/269673599554551808/1093631106370248734) - [Image 1](./mirrors/script-29.png)

```clientscript
if (map_playercount(0_31_70_00_00, 0_31_70_20_27) > 0) {
  if (map_playercount(0_31_70_42_00, 0_31_70_62_27) > 0) {
    if (map_playercount(0_31_70_00_35, 0_31_70_20_62) > 0) {
      if (map_playercount(0_31_70_42_35, 0_31_70_62_62) > 0) {
        if (map_playercount(1_31_70_00_00, 1_31_70_20_27) > 0) {
          if (map_playercount(1_31_70_42_00, 1_31_70_62_27) > 0) {
            if (map_playercount(1_31_70_00_35, 1_31_70_20_62) > 0) {
              if (map_playercount(1_31_70_42_35, 1_31_70_62_62) > 0) {
                if (map_playercount(2_31_70_00_00, 2_31_70_20_27) > 0) {
                  if (map_playercount(2_31_70_42_00, 2_31_70_62_27) > 0) {
                    if (map_playercount(2_31_70_00_35, 2_31_70_20_62) > 0) {
                      if (map_playercount(2_31_70_42_35, 2_31_70_62_62) > 0) {
                        if (map_playercount(3_31_70_00_00, 3_31_70_20_27) > 0) {
                          if (map_playercount(3_31_70_42_00, 3_31_70_62_27) > 0) {
                            if (map_playercount(3_31_70_00_35, 3_31_70_20_62) > 0) {
                              if (map_playercount(3_31_70_42_35, 3_31_70_62_62) > 0) {
                                // all 16 cutscene rooms are full!!
```

## script-30

[Original](https://twitter.com/PhoenixHadyn/status/1673496983300874240)

> @PhoenixHadyn: @JagexAsh Is the welcome message "Welcome to Old School RuneScape." handled by the engine, or is it
> implemented in RuneScript?

> @JagexAsh: RuneScript. That's how it can say different things in ScapeRune or on certain special worlds.

> @TheCrazy0neTv: What script trigger is the main message coming from?

> @JagexAsh: The login trigger. Why?

> @TheCrazy0neTv: Just curious, the message seems to be sent super early on compared to a lot of other things the server
> sends.

> @JagexAsh: The login trigger's the first thing it does, so it seems reasonable that the message would arrive early on.
> Other things are often queued from login rather than being executed instantly, such as rejoining channels.

## script-31

[Original](https://twitter.com/PhoenixHadyn/status/1570211140491268099)

> @PhoenixHadyn: @JagexAsh Hey ash! You previously mentioned that `ai_queue3` is used to handle death for NPCs. Is that
> automatically triggered by the engine? Or in other words, is it possible to trigger ai_queue from RuneScript itself?

> @JagexAsh: It's triggered via RuneScript. The command would be `npc_queue(3,0,0)` - the first zero is an integer
> parameter that could be used for passing in extra info, though it's seldom needed, and the second is a number of ticks
> before the queue should be allowed to execute.

> @PhoenixHadyn: Thanks that makes sense. Is there any reasoning behind 3 being for death? Is it like for priority? e.g:
> other stuff have to be run before death so they take `ai_queue1` and `ai_queue2`

> @JagexAsh: 1 is used for retaliation. 2 was used for applying damage, though I replaced it a few years ago when I did
> the rewrite for tinted hitsplats. 4-20 are also available, but 1-3 are the common ones with really standardised uses.

> @TheCrazy0neTv: If ai_queue only allowed passing a single int to the script, how did the `ai_queue2` for applying
> damage work if it couldn't access the player applying the damage for credit since there was no way to pass them?

> @JagexAsh: The credit and XP were handled in the script that set the queue, rather than in the queue itself.<br>From
> that you can see why XP arrives earlier than the damage, btw.

> @TheCrazy0neTv: Guessing the tinted hitmarks required you to have an active player when applying it? Also, are you
> able to share how applying the damage credit to an npc might look like?

> @JagexAsh: They do; that's why we couldn't work on the tinted splats job until we got the new queue type. If the devs
> in 2004 had had that queue type, maybe they'd have done XP within it too, rather than giving that up
> front.<br>`npc_heropoints($damage)`
