# Varobj

This page documents all known `varobj` related releases.

## varobj-1

[Original](https://twitter.com/TheCrazy0neTv/status/1648265015978254337) - [Mirror](./mirrors/varobj-1.png)

> @TheCrazy0neTv: @JagexAsh I once saw the 'objvar' property on objs. I was wondering how that works for something like
> the Toxic blowpipe since it stores 3 different variables, but as far as I know items can only store up to 32 bits of
> data total?

> @JagexAsh: One of those variables is a dart quantity, another corresponds to a dart type, a third is a quantity of
> flakes. The 32 bits are allocated between the three of them.

> @TheCrazy0neTv: Where are the number of bits each one takes up defined? The example I saw using that property just
> listed some names and that was it (example I saw was for the Bonecrusher).

> @JagexAsh: 4 for the type of dart, 14 each for the other two.

> @TheCrazy0neTv: How are those bit amounts defined, is there a .varobj file or something?

> @JagexAsh: Exactly that.
> ```
> // What kind of darts are in here?
> [snakeboss_blowpipe_darttype]
> startbit=0
> endbit=3
> 
> // How many darts are in here?
> [snakeboss_blowpipe_dartcount]
> startbit=4
> endbit=17
> 
> // How many more flakes has it got?
> [snakeboss_blowpipe_flakes]
> startbit=18
> endbit=31
> ```

> @PhoenixHadyn: Out of curiosity, do you know the exact number of .varobj are there currently? Or an estimate I guess.

> @eep_cabbage: I count 101 .varobj files, seen in config group 20

> @JagexAsh: Yup, confirmed.
