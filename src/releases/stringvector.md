# String Vector

This page documents all known `stringvector` related releases.

## stringvector-1

[Original](https://twitter.com/TheCrazy0neTv/status/1656289730420523010)

> @TheCrazy0neTv: @JagexAsh What is the new type being used in clientscript with the introduction of the loot tracker?

> @JagexAsh: stringvector - it's a new kind of string type used for the comma-separated lists in the filters.

> @TheCrazy0neTv: Ah very interesting, I was thinking it looked like some list-like thing. Are new stringvectors defined
> in engine or is there a new config associated with them?

> @JagexAsh: It's a new config. I've not used it myself.

> @TheCrazy0neTv: Do you know if they are only usable in the client, or can the server also interact with them?

> @JagexAsh: Client only, as far as I know, though the server can tell the client to write to them. They're supposed to
> be similar to .varc configs that we already had, which are variables (mostly integers) stored on your device.
