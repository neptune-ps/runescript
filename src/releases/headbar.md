# Headbar

This page documents all known `headbar` related releases.

## headbar-1

[Original](https://twitter.com/kgstaschke/status/1654122326764707841)

> @kgstaschke: @JagexAsh, another quick question. could you provide an example of a .headbar configuration that
> showcases its capabilities? I'm curious to learn about all the properties that can be defined using it. Thank you in
> advance.

> @JagexAsh:
> ```
> [health_default]
> full= [graphic of full bar]
> empty= [graphic of empty bar]
> sticktime= [duration in 0.02s client cycles, usually 300]
> fadeout= [no or yes, usually no]
> segments= [number of distinct values it can show, usually 30]
> ```

> @PhoenixHadyn: On this topic, is it possible to manage the order of headbars in OSRS through the config? I remember
> that in RuneScape 3, some headbars had higher priority than others.

> @JagexAsh: We'd likely have the same priority option they do - backports are usually complete (aside from what we got
> for our World Map) - though I don't think OSRS is doing anything with it at present.
