# Obj

This page documents all known `obj` related releases.

## obj-1

[Original](https://www.youtube.com/watch?v=5pvoMQUCla4)

> Title: OSRS Design Stream with Mod West - Kebos Lowlands Rewards<br>
> Date: 2018-11-09

### obj-1-0 (`diary_rewards.obj`)

```
...
param=morytania_legs,4
param=ghostspeak,1
param=telegrab_disabled,true
param=wear_op1,Ectofuntus Pit
param=wear_op2,Burgh de Rott

[bonecrusher]
name=Bonecrusher
desc=Crushes bones.
model=bonecrusher
2dzoom=905
2dxof=5
2dyof=-91
2dxan=269
2dyan=281
iop2=Check
iop3=Activity
iop4=Uncharge
iop5=Destroy
objvar=charges,bonecrusher_deactivated
members=yes
tradeable=no
stockmarket=yes
playercost=80
// This item can sit on the ground to be reclaimed with its charges intact.
param=destroyondrop,^false
param=telegrab_disabled,true
weight=25g
param=no_alchemy,^true

[falador_shield_easy]
name=Falador Shield 1
desc=A shield from Falador.
model=inv_human_weapons_falador_shield_easy
2dzoom=1100
2dxof=1
2dyof=1
2dxan=500
2dyan=600
iop2=Wear
iop3=Recharge-prayer
iop4=Check
cost=0
members=yes
tradeable=no
stockmarket=no
playercost=10
param=prayerbonus,1
param=stabattack,0
...
```

### obj-1-1 (`brimstone.obj`)

Note: The `model` property of `brimstone_ring` was modified within the same source. Both values are shown.

```
...
2dyan=1926
2dzan=1697
2dxan=269
cost=1500000
members=yes
tradeable=no
stockmarket=no
playercost=1000000

[hydra_eye]
//Needs editing
name=Hydra's claw
desc=The eye of a hydra, looks like it can be combined with a heart and a fang.
weight=1g
model=inv_newtseye
2dxof=1
2dyof= TODO
2dzoom=690
2dyan=256
2dxan=364
cost=1500000
members=yes
tradeable=no
stockmarket=no
playercost=1000000

[brimstone_ring]
name=Brimstone Ring
desc=A well balanced ring that aids in piercing magic defence.
cost=10000
respawnrate=500
model=area_dagganoth_dungeon_inv_ring_archer
model=inv_brimstone_ring
iop2=Wear
wearpos=ring
weight=4g
param=rangeattack,4
param=rangedefence,4
param=stabattack,4
param=stabdefence,4
param=slashattack,4
param=slashdefence,4
param=stabattack,4
param=stabdefence,4
param=crushattack,4
param=crushdefence,4
param=magicattack,6
param=magicdefence,6
param=strengthbonus,4
members=yes
2dxof=4
2dyof=-14
2dzoom=630
2dyan=1904
2dxan=332
tradeable=yes
stockmarket=yes
stockmarketbuylimit=^ge_ratelimit_combatkit_rare
stockmarketrecalcusers=^ge_recalcusers_low
playercost=3200000
param=magic_pierce,1

[dragonhunter_lance]
name=Dragon hunter lance
desc=A lance that is exceptionally good at killing dragons.
cost=1000000
model=inv_dhunter_lance
iop2=Wield
wearpos=righthand
manwear=human_weapons_dragonhunter_lance,0
womanwear=human_weapons_dragonhunter_lance,6
2dzoom=1516
2dyan=1428
2dzan=202
2dxan=673
category=weapon_spear
param=strengthbonus,70
param=prayerbonus,0
param=attackrate,4
param=attackrange,0
param=stabattack,85
param=slashattack,65
param=crushattack,65
param=rangeattack,0
param=magicattack,0
param=stabdefence,0
param=slashdefence,0
param=crushdefence,0
param=rangedefence,0
param=stabattack_anim,human_dhunter_lance_attack
param=slashattack_anim,human_dhunter_lance_slash
param=crushattack_anim,human_dhunter_lance_crush
param=defend_anim,human_stafforb_block
weight=3kg
param=stab_sound,staff_stab
param=slash_sound,scythe_slash
param=crush_sound,newstaff
param=equip_sound,equip_staff
param=ready_baseanim,human_ready_scythe
param=turnonspot_baseanim,human_halberdturnonspot
param=walk_f_baseanim,human_halberdwalk_f
param=walk_b_baseanim,human_halberdwalk_b
param=walk_l_baseanim,human_halberdwalk_l
param=walk_r_baseanim,human_halberdwalk_r
param=running_baseanim,human_dh_weapon_run
param=zamorakitem,1
members=yes
tradeable=yes
stockmarket=yes
stockmarketbuylimit=^ge_ratelimit_combatkit_rare
stockmarketrecalcusers=^ge_recalcusers_normal
playercost=2000000
param=dragonbane_bonus,30

[ferocious_gloves]
name=Ferocious gloves
desc=A pair of very very nice gloves.
//this model needs changed
model=quest_hundred_100_james_inv_gloves1
2dyof=-1
2dzoom=789
2dyan=111
2dxan=609
weight=8oz
param=strengthbonus,14
param=stabattack,16
param=crushattack,16
param=rangeattack,-16
param=magicattack,-16
param=stabdefence,0
param=slashdefence,0
param=crushdefence,0
param=rangedefence,0
param=magicdefence,0
iop2=Wear
wearpos=hands
//these models need changed
manwear=man_hands_100_gloves1,0
womanwear=woman_hands_100_gloves1,0
category=armour_hands
param=equip_sound,equip_hands
param=convertondrop,hydra_leather
cost=100000
members=yes
tradeable=no
stockmarket=no
playercostderived=hydra_leather
playercostderived_const=300

[hydra_leather]
name=Hydra Leather
desc=Hide from a ferocious Hydra, Erdan from the Myth's Guild may know what to do with this.
model=inv_cowhide
2dxof=-1
2dyof=-2
2dzoom=1190
2dyan=116
2dxan=440
cost=1500000
weight=8oz
members=yes
tradeable=yes
stockmarket=yes
stockmarketbuylimit=^ge_ratelimit_combatkit_uncommon
stockmarketrecalcusers=^ge_recalcusers_normal
playercost=1000000

[bonecrusher_necklace]
name=Bonecrusher Necklace
desc=A necklace made out of dragon bones and a bonecrusher.
model=inv_dragonbone_necklace
2dxof=1
2dyof=20
2dzoom=716
2dyan=81
2dxan=417
2dzan=27
wearpos=front
manwear=man_necklaces_dragonbone,0
womanwear=man_necklaces_dragonbone,0
param=prayerbonus,12
cost=80000
members=yes
tradeable=yes
stockmarket=yes
iop2=Wear
iop1=Check
iop3=Activity
iop4=Uncharge
iop5=Dismantle
objvar=charges,bonecrusher_necklace_deactivated
members=yes
tradeable=no
stockmarket=no
playercost=80
// The item can sit on the ground to be reclaimed with its charges intact.
...
```

## obj-2

[Original](https://twitter.com/TheCrazy0neTv/status/1608156459912818688)

> @TheCrazy0neTv: @JagexAsh What's the difference between cost and playercost in an obj config?

> @JagexAsh: 'cost' determines shop prices and alch prices.<br>'playercost' sets the initial GE value for tradeable
> items (and is then ignored if it changes later on, since those follow trade trends thereafter) and sets the death
> protection value for untradeables.

## obj-3

[Original](https://twitter.com/JagexAsh/status/567679690140184576) - [Mirror](./mirrors/obj-3.png)

> @JagexAsh: Another odd entry from the @OldSchoolRS item definitions. I'd really hate to think someone got those words
> confused.

```
[Book_of_Astrology]
name=Astronomy book
desc=A book on the history of astronomy.
model=qip_obs_inv_astronomy_book
2dxof=2
2dyof=10
2dzoom=950
2dyan=1948
2dxan=260
ambient=15
weight=3g
cost=3
iop1=Read
members=yes
tradeable=yes
stockmarket=no
playercost=1
```

## obj-4

[Original](https://twitter.com/OsrsScaping/status/1117689329067839489) - [Mirror](./mirrors/obj-4.png)

> @OsrsScaping: @JagexAsh Why are Gout Tubers so rare?

> @JagexAsh: The dev who made it is no longer around to ask, though the item definition he wrote suggests that he really
> meant it.

```
[Village_Rare_Tuber]
name=Gout Tuber
desc=Plant this in a herb patch to grow Goutweed.
cost=200
model=area_tbw_cleanup_inv_rare_tuber
```

## obj-5

[Original](https://twitter.com/JagexAsh/status/565868420202496000) - [Mirror](./mirrors/obj-5.png)

> @JagexAsh: Found this in the @OldSchoolRS item definition files. Were you one of the noobs?

```
//Creator:  XXXXXXXXXXXXXXX
//Date:     24/03/05
//Purpose:  To fool all the noobs on April Noob Day

[spinning_plate]
name=Spinning plate
desc=It has a picture of a dragon on it.
cost=50
weight=50g
iop1=Spin
model=inv_spinning_plate
```

## obj-6

[Original](https://twitter.com/JagexAsh/status/566661644408082433) - [Mirror](./mirrors/obj-6.png)

> @JagexAsh: Deep in the @OldSchoolRS item definition files, I've just found this long-forgotten example of programmer
> humour.

```
//--------------------------------------- object oriented spears ;)

[Bronze_spear_p++]
name=Bronze Spear(p++)
desc=A poisoned bronze tipped spear.
members=yes
iop2=Wield
```

## obj-7

[Original](https://twitter.com/JagexAsh/status/1121117398801756160) - [Mirror](./mirrors/obj-7.png)

> {deleted tweet}

> @JagexAsh: Nah, just chill.<br>Ask your son if you require assistance with that.

> @OmniGoku: Ash gonna smoke the grass I support

> @JagexAsh: You know the Ratcatchers quest? There's an item in that quest that suggests its developer had a similar
> sense of humour to you, even if the player-facing text was made more mundane.

```
[ratcatchers_smokey_weedpot]
name=Smouldering pot
desc=Contains slowly burning garden weeds.
model=quest_ratcatchers_inv_smoking_pot
weight=500g
```

## obj-8

[Original](https://twitter.com/JagexAsh/status/569561056177225728) - [Mirror](./mirrors/obj-8.png)

> @JagexAsh: This item definition file just brightened my afternoon - behold the rare Left-handed Banana of
> @OldSchoolRS!

```
// Following banana can be wielded in the left hand:
[banana_left]
name=Left-handed banana
desc=Mmm this looks tasty.
cost=1
model=inv_bannana
2dxof=9
2dyof=10
2dzoom=930
2dyan=1944
2dxan=280
weight=1oz
wearpos=lefthand
manwear=human_weapons_bannana_left,0
womanwear=human_weapons_bannana_left,6
members=no
```

## obj-9

[Original](https://twitter.com/JagexRy/status/854345279646126083) - [Mirror](./mirrors/obj-9.jpg)

> @JagexRy: The Infernal cape is ready but you'll have to wait a couple more days to see it. Reveal will be on this
> Thursday's Q&A!

```
[infernal_cape]
name=Infernal cape
desc=A cape of... something.
iop2=Wear
wearpos=back
```

## obj-10

[Original](https://twitter.com/TheCrazy0neTv/status/1613998724304277521) - [Mirror](./mirrors/obj-10.png)

> @TheCrazy0neTv: @JagexAsh What are all of the possible wearpos values? So far I've seen lefthand, righthand, back,
> front, ring, and hands, but I believe these are maybe only half of them.

> @JagexAsh: In addition to the obvious 11 equip slots, items can optionally replace your arms, your head (in addition
> to your hat slot) and your jaw (causing beards to disappear when equipped).

> @TheCrazy0neTv: Could you show the ^wearpos_ constants?

> @JagexAsh: _head, _jaw and _arms are almost never used except for defining items that replace those models from your
> base appearance. Though I invented a weird hack for putting items in _jaw to get team cape effects without using the
> cape slot. It's now used for BA icons too, etc.

```
^wearpos_hat      =  0
^wearpos_back     =  1
^wearpos_front    =  2
^wearpos_rhand    =  3
^wearpos_torso    =  4
^wearpos_lhand    =  5
^wearpos_arms     =  6
^wearpos_legs     =  7
^wearpos_head     =  8
^wearpos_hands    =  9
^wearpos_feet     = 10
^wearpos_jaw      = 11
^wearpos_ring     = 12
^wearpos_quiver   = 13
```

> @TheCrazy0neTv: Thank you very much! Also, that's very interesting. Does that mean you need a new item per beard then?
> Seems like a lot of fun to upkeep!

> @JagexAsh: At least one new item, yes. For example, each beard requires an item for each of the 4 BA icons.<br>It'll
> be a slight nuisance for future beard option expansions, but I reckoned it'd be worth it.

## obj-11

[Original](https://twitter.com/TheCrazy0neTv/status/1676690820429225985)

> @TheCrazy0neTv: @JagexAsh I've seen "dummyitem" referenced a few times for obj configs, I was curious what the
> possible values of that property are as they appear to support 3 different values so it might not just be a yes/no
> property?

> @JagexAsh: `graphic_only` - item can never be spawned or put in inventories. Used for the fake items that appear in
> your hand during animations.<br>`inv_only` - item can be put in dummy inventories, but not real ones, nor can it be
> spawned. Dummy inventories aren't much used now.

> @TheCrazy0neTv: Aha, thank you. You mentioned that placeholder items were set to some sort of "dummyitem" but they
> don't seem like they'd fit either of those since you need to spawn them in the bank, or is the bank a dummy inventory?

> @JagexAsh: Placeholders inherit some of their attributes from a dummy item, but they are not themselves dummy items.
> Banknotes do likewise, inheriting their appearance and stackability from a dummy item, but they're not dummy
> themselves either.

## obj-12

[Original](https://twitter.com/kgstaschke/status/1653925675001061377)

> @kgstaschke: @JagexAsh Hey there. Curious, in RuneScript (configuration) how do you specify what option does the shift
> key operate on? Is it like shiftop=3? Thanks man!

> @JagexAsh: `shiftclickiop=3`<br>Inventory options (such as "Drop") are referred to via `iop`, whereas ground options (
> such as "Take") are referred to via `op`. Though there's no `shiftclickop` because we don't use ground items that way,
> so far.

## obj-13

[Original](https://twitter.com/li_bert/status/1625903716300890113)

> @li_bert: @JagexAsh mod ash what is the process of making something not stackable to stackable? In terms of coding :)
> ily friend

> @JagexAsh: Either you change the `stackable=no` to `stackable=yes`, then write code to handle players who'd got them
> in inventories already (plus banknotes if the item was tradeable) or you define a separate stackable item and program
> it to work the same as the original one.
