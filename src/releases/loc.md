# Loc

This page documents all known `loc` related releases.

## loc-1

[Original](https://twitter.com/PhoenixHadyn/status/1640464372341305351)

> @PhoenixHadyn: @JagexAsh What property is used to prevent stuff like stairs from being interacted from all directions?
> And how do you specify directions?

> @JagexAsh: It's called `forceapproach`, specified as north, south, east or west.<br>Scenery can be rotated;
> the `forceapproach` property is relative to the scenery's orientation. So if we put `forceapproach=north`, then rotate
> the scenery 90 degrees clockwise, it'll be usable from the east.

> @PhoenixHadyn: That's interesting. Would you also happen to know what property controls whether an object can be
> placed on top of a location (e.g: tables)?

> @JagexAsh: `raiseobject` true/false<br>Remind me, what do you even do with this info? If you were writing your own
> game engine, and you wanted some scenery to make items go above it while others didn't, surely you could just name
> your toggle anything you liked?

## loc-2

[Original](https://twitter.com/TheCrazy0neTv/status/1773181687372935266)

> @TheCrazy0neTv: @JagexAsh What's the new loc config property that was added with the most recent update? It seems to
> have something to do with loc_anim command potentially.

> @JagexAsh: `randomanimframe=BOOLEAN` is a pretty recent LOC addition involving animations. It defaults to TRUE.<br>If
> TRUE, the LOC performs its animation from a random point in the sequence, rather than always starting at the
> beginning, e.g. so that fires don't all synchronise their flames.

> @TheCrazy0neTv: Ah that isn't the one I meant, but I was curious about that one as well. The one I meant was only
> added with the last engine update, and potentially changes some behavior when doing loc_anim after recently doing
> loc_change.

> @JagexAsh: Previously, `loc_anim()` simply didn't act if the LOC had changed/spawned in the last tick; there was a
> change to allow it to do so. The only other new LOC-related change I know about is creating `loc_anim_specific()`,
> which animates a LOC only for a particular player. Sorry.
