# Seq

This page documents all known `seq` related releases.

## seq-1

[Original](https://twitter.com/PhoenixHadyn/status/1643781596946870273) - [Mirror](./mirrors/seq-1.png)

> @PhoenixHadyn: @JagexAsh Hey Ash! Would you share some insights on ".seq" config format? They seem to have more
> properties than just "animation frame"s (on previous development streams, it seemed ".anim" file is all you needed to
> animate a model)

> @JagexAsh: Animation frames are graphical files created by the animators. A .seq (sequence) file can list what order
> those frames should be played, and the duration of each frame, allowing the graphical files to be used flexibly,
> without the need to duplicate them if a stance is repeated.

> @PhoenixHadyn: Ah thanks, that makes more sense. Mind sharing what that looks like? (e.g:
> frame=anim_frame,frame_duration)<br><br>I am a bit curious as that may cause the .seq config to be quite large given
> animation frames can be up to a thousand frames or more per .anim.

> @JagexAsh: 'ready' stance for the new equippable cards:<br>I don't recall any as big as you describe, though the
> artists may have expanded their work as their tools improve.
> ```
> [human_card_ready]
> base=human
> frame1=human_card_ready_f1
> delay1=4
> frame2=human_card_ready_f9
> delay2=4
> frame3=human_card_ready_f5
> delay3=4
> frame4=human_card_ready_f10
> delay4=4
> frame5=human_card_ready_f3
> delay5=4
> frame6=human_card_ready_f11
> delay6=4
> frame7=human_card_ready_f6
> delay7=4
> frame8=human_card_ready_f12
> delay8=4
> frame9=human_card_ready_f2
> delay9=4
> frame10=human_card_ready_f13
> delay10=4
> frame11=human_card_ready_f7
> delay11=4
> frame12=human_card_ready_f14
> delay12=4
> frame13=human_card_ready_f4
> delay13=4
> frame14=human_card_ready_f15
> delay15=4
> frame15=human_card_ready_f18
> delay15=4
> ```

> @PhoenixHadyn: I see, thanks, and I guess that also contains other properties such as mainhand and offhand, priority,
> loop etc.?

> @JagexAsh: Correct :)

## seq-2

[Origina](https://twitter.com/TheCrazy0neTv/status/1644304490386935808)

> @TheCrazy0neTv: Is the frame name based on the .anim file name + the frame id appended to it, or does the artist
> choose a name for each frame within an .anim file?

> @JagexAsh: The frames listed here are individual frame files, each named by the artist when they saved it. (Though in
> practice they've always had tools that'll save lots of them at once, rather than typing it manually each time.)

## seq-3

[Original](https://twitter.com/PhoenixHadyn/status/1645779991207215108)

> @PhoenixHadyn: @JagexAsh How do you make an animation work with other animations (e.g: walking while doing some other
> animation at the same time). It seems to be per sequence, is there a property that controls this?

> @JagexAsh: It's called `walkmerge`, specified for an animation via the .seq files that you may have seen. The animator
> specifies which labels of the entity's model should be excluded from the animation, leaving them free to perform the
> entity's walk-anim instead.

> @PhoenixHadyn: Thanks, that makes more sense. Do you need to specify that for every label you want to use the walk
> sequence? (e.g: `walkmerge=left_foot`, `walkmerge=right_foot`) or are there like presets that you can do (e.g:
> `walkmerge=some_preset`) which include all the labels to avoid repetition

> @Jagexash: You have to list every label - but it's in a comma-separated list on one line, rather than multiple
> lines.<br>I asked about getting that changed to use presets, years ago, though the engine team has always had other
> work to consider that'd bring more benefit for artists & players.

## seq-4

[Original](https://twitter.com/PhoenixHadyn/status/1715778112984670420)

> @PhoenixHadyn: Hey @JagexAsh. What property controls whether animation or movement is stalled when performing an animation? Some animations tend to wait for movement to complete while others do not.

> @JagexAsh: Setting in the "sequence" definition for each animation.

> @PhoenixHadyn: Thanks! Would you mind sharing what that property is called and what potential values it can have?

> @JagexAsh: 
> ```
> postanim_move=
> abortanim (cancel animation)
> delaymove (stall movement)
> merge (skid while animating)
> preanim_move=
> delayanim (finish move, then animate)
> delaymove (stall movement)
> merge (skid while animating)
> ```
