# Area Documentation

Areas can be "painted" on individual tiles which enables the `inarea(area, coord)` RuneScript command, and allows
optionally triggering scripts when the player enters or leaves an area. Each tile can have up to 5 areas painted on it.

## Properties

Areas are a server-only config therefore none of the properties are transmitted to the client.

| Name      | Description                                                                |
|-----------|----------------------------------------------------------------------------|
| `colour`  | The color to highlight a tile when viewed in the map editor.               |
| `trigger` | Whether or not the area should trigger a script when entering and leaving. |
