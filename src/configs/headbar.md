# Headbar Documentation

Headbars are a config that allow configuring different looks for headbars used for players and npcs.

## Properties

The follow table lists all known properties used in a headbar config. Properties marked with an asterisk (`*`) are
server only.

| Name        | Description                                |
|-------------|--------------------------------------------|
| `full`      | Graphic of full bar.                       |
| `empty`     | Graphic of empty bar.                      |
| `sticktime` | Duration in client cycles.                 |
| `fadeout`   | Makes the headbar fadeout over time.       |
| `segments`  | The number of distinct values it can show. |
