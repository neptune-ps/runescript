# Param Documentation

Params allow storing arbitrary static data on other configs that support them.

## Properties

The follow table lists all known properties used in a param config. Properties marked with an asterisk (`*`) are server
only.

| Name          | Description                                                           |
|---------------|-----------------------------------------------------------------------|
| `transmit`*   | Whether to transmit the param to client.                              |
| `type`        | The type of the param.                                                |
| `autodisable` | Whether to remove the param when in a free to play server.            |
| `default`     | The default value of the param if it isn't defined on another config. |
